import Vue from "vue";
import Vuex from "vuex";
import db from './db'
import router from '../router/index'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authenticated : false,
    restaurantName : "",
    simpleMenu : [], 
    users : [],
    shoppingCart : 0,
    totalAmount : 0
  },
  // fonctions qui modifies le state
  mutations: {

    LOG_IN(state, {id, mdp}){
        if(id != "" && mdp != "") {
          for(let user of state.users){
            if(user.login  == id && user.password== mdp) {
                state.authenticated = true
                router.replace({ path : "/" });
            }
          }
        }
    },

    LOG_OUT(state){
      state.authenticated = false;
    },
    
    SET_RESTNAME (state, restaurantName) {
      state.restaurantName = restaurantName
    },
    SET_MENU (state, menu) {
      state.simpleMenu = menu.items
    },
    SET_USERS(state, users){
      state.users = users.user
    },

    ADD_ITEMS_TO_PANIER(state, { amount, name }) {
      for (let item of state.simpleMenu){
        if(amount <= item.quantityInStock) {
          if(item.name === name && item.panierQuantite!= undefined ){
            state.shoppingCart += amount
            item.panierQuantite += amount
            item.quantityInStock -= amount
          } else if(item.name === name && item.panierQuantite == undefined ){
            state.shoppingCart += amount
            item.panierQuantite = amount
            item.quantityInStock -= amount
          }
        }
      }
      
		},
    UPDATE_PANIER_TOTAL(state){
      state.totalAmount = 0
      for (let item of state.simpleMenu){
        if (item.panierQuantite != undefined) {
          state.totalAmount += item.panierQuantite * item.price
        }
      }
    }, 

  

    /*SUPP_ITEM_STOCK(state){
    for(let item of state.simpleMenu){
      pouchDB.put({
        _id : "menus" ,
        _rev : "19-0192d340df1ab2c9b4cdde828a46c8c0", 
        menu: {
          items : [

          ]
        }
      })
    }*/
  },

  actions: {

    async initState (context) {
      try {
        const resNameRestaurant = await db.get('infos-restaurant');
        context.commit('SET_RESTNAME', resNameRestaurant.restaurantName)
      } catch (err) {
        // eslint-disable-next-line
        console.log(err)
      }

      try {
        const resMenu = await db.get('menus');
        context.commit('SET_MENU', resMenu.menu)
  
      } catch (err) {
        // eslint-disable-next-line
        console.log(err)
      }

      try {
        const resUser = await db.get('users');
        context.commit('SET_USERS', resUser.users)
  
      } catch (err) {
        // eslint-disable-next-line
        console.log(err)
      }
      
    },

    updateQuantityInPanier({ commit }, { amount, name }) {
      commit("ADD_ITEMS_TO_PANIER", {amount : amount, name : name }) 
    },

    updatePanierTotal({commit}){
      commit("UPDATE_PANIER_TOTAL")
    }, 

    supprimerItemStock({commit}){
      commit("SUPP_ITEM_STOCK")
    },

    login({ commit }, { id, mdp }) {
      commit("LOG_IN", {id, mdp})
    },
    logout({commit}) {
      commit("LOG_OUT")
    }
  },

  getters : {
    getQuantityItem: (state) => (name) => {
      for (let item of state.simpleMenu){
        if(item.name === name){
          return item.panierQuantite
        }
      }
      return 0
    }
  },

  modules: {
  }, 

})

