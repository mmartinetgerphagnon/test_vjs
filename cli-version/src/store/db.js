import PouchDB from 'pouchdb-browser'
import PouchDBFind from 'pouchdb-find'
PouchDB.plugin(PouchDBFind)

// let permet de déclarer une variable dont la portée est celle
// du bloc courant 
let dbUrl 


dbUrl = `http://admin:auragen@localhost:5984/menu-item`

var db = new PouchDB(dbUrl)

export default db
