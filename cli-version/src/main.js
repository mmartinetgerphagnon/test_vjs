import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// genere le code HTML 
// charge le code HTML genere dans la section <div id='app'> </div>
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
